def mini(data,dic):
    list=[]
    kk=dic['columns'][1]
    str=kk[0]
    strip1=str.split("min(")
    strip2=strip1[1].split(')')
    col=strip2[0]
    table=dic['tables'][0]
    for x in data:
        if table==data[x]['name']:
            #print(x)
            for y in data[x]['columns']:
                #print(y)
                if col==y:
                    index=data[x]['columns'].index(y)
            for y in data[x]['values']:
                list.append(y[index])
    value=min(list)
    print(value)

def maxi(data,dic):
    list=[]
    kk=dic['columns'][1]
    str=kk[0]
    strip1=str.split("max(")
    strip2=strip1[1].split(')')
    col=strip2[0]
    table=dic['tables'][0]
    for x in data:
        if table==data[x]['name']:
            #print(x)
            for y in data[x]['columns']:
                #print(y)
                if col==y:
                    index=data[x]['columns'].index(y)
            for y in data[x]['values']:
                list.append(y[index])
    value=max(list)
    print(value)

def sum(data,dic):
    list=[]
    kk=dic['columns'][1]
    str=kk[0]
    strip1=str.split("Sum(")
    strip2=strip1[1].split(')')
    col=strip2[0]
    table=dic['tables'][0]
    for x in data:
        if table==data[x]['name']:
            #print(x)
            for y in data[x]['columns']:
                #print(y)
                if col==y:
                    index=data[x]['columns'].index(y)
            for y in data[x]['values']:
                list.append(y[index])
    value=0
    for x in list:
        value += x
    print(value)

def average(data,dic):
    list=[]
    kk=dic['columns'][1]
    str=kk[0]
    strip1=str.split("average(")
    strip2=strip1[1].split(')')
    col=strip2[0]
    table=dic['tables'][0]
    for x in data:
        if table==data[x]['name']:
            #print(x)
            for y in data[x]['columns']:
                #print(y)
                if col==y:
                    index=data[x]['columns'].index(y)
            for y in data[x]['values']:
                list.append(y[index])
    value=0
    for x in list:
        value += x
    value=value/len(list)
    print(value)
