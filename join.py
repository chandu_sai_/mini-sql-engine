def combine_tables(data,table1,table2):
	combined_data = {}
	combined_data['columns'] = []
	combined_data['values'] = []
	row  = data[table1]['columns']
	for i in range(len(row)):
		x = table1+'.'+row[i]
		combined_data['columns'].append(x)
	row  = data[table2]['columns']
	for i in range(len(row)):
		x = table2+'.'+row[i]
		combined_data['columns'].append(x)
	for row in data[table1]['values']:
		arr = row[:]
		for row2 in data[table2]['values']:
			arr[:] = row[:]
			for i in range(len(row2)):
				arr.append(row2[i])
			combined_data['values'].append(arr)
			arr = []
	return combined_data

def join(data,dic):
    combined_data=dict()
    line=dic['cond'][2]
    line1=line.split("=")
    table1s=line1[0].split('.')
    table2s=line1[1].split('.')
    table1d=table1s[0]
    table2d=table2s[0]
    combined_data=combine_tables(data,table1d,table2d)
    for x in combined_data['columns']:
        if line1[0]==x:
            #print(x)
            index1=combined_data['columns'].index(x)
    for x in combined_data['columns']:
        if line1[1]==x:
            #print(x)
            index2=combined_data['columns'].index(x)
    required=[]
    required=dic['columns'][1]
    required_tables=[]
    required_columns=[]
    required_tables=dic['tables']
    #print(required_tables)
    if len(required)!=len(required_tables):
        print("ERROR:--TABLES AND COLUMNS DOESN;T MATCH")
        return
    for x in (0,1):
        required_columns.append(required_tables[x]+"."+required[x])
    required_indices=[]
    for x in required_columns:
        required_indices.append(combined_data['columns'].index(x))
    #print(required_indices)
    tup=[]
    tuples=[]
    print(", ".join(required_columns))
    for x in combined_data['values']:
        if x[index1]==x[index2]:
            tup=[x[i] for i in required_indices]
            if tup not in tuples:
                tuples.append(tup)
    for z in tuples:
        printer=', '.join(str(v) for v in z)
        if printer!="":
            print(printer)
    return
