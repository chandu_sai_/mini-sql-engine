def query_check(query,query_array):
    less_length=len(query)
    length=len(query_array)
    run=query_array[len(query_array)-1].split(';')
    query_array[len(query_array)-1]=run[0]
    flag=1;
    if query[less_length-1]!=';':
        print("ERROR:--INVALID SYNTAX--")
        flag=0
        return flag
    if length<4 or (length==5 and "where" in query):
        print("ERROR:--INVALID SYNTAX--")
        flag=0
        return flag
    if length>4:
        if "where" not in query_array:
            flag=0
            print("ERROR:--INVALID QUERY-- condition not stated clearly")
            return flag
    if query_array[0]!="Select":
        print("ERROR:--SYNTAX IS INCORRECT-- query must start with Select")
        flag=0
        return flag
    if query_array[2]!="from":
        print("ERROR:--SYNTAX IS INCORRECT-- path was not mentioned to print columns")
        flag=0
        return flag
    # if query_array.index('from') == length-1 or query_array[query_array.index('from')+1] == 'where':
	# 	print('ERROR:--TABLE NOT MENTIONED--')
    #     flag=0
	# 	return flag
    return flag
