def con(data,dic,table_name,flag):
    if dic['cond'][0]==0:
        condition1=dic['cond'][2]
        list=[]
        if "<=" in condition1:
            smash=condition1.split("<=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]<=value:
                            list.append(y)
                    data[x]['values']=list
        elif ">=" in condition1:
            smash=condition1.split(">=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]>=value:
                            list.append(y)
                    data[x]['values']=list
        elif "<" in condition1:
            smash=condition1.split("<")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]<value:
                            list.append(y)
                    data[x]['values']=list
        elif ">" in condition1:
            smash=condition1.split(">")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]>value:
                            list.append(y)
                    data[x]['values']=list
        elif "=" in condition1:
            smash=condition1.split("=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]==value:
                            list.append(y)
                    data[x]['values']=list
        return data
    elif dic['cond'][0]==1 and dic['cond'][1]==1 and flag==1:
        condition1=dic['cond'][2]
        condition2=dic['cond'][3]
        list=[]
        if "<=" in condition1:
            smash=condition1.split("<=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]<=value:
                            list.append(y)
                    data[x]['values']=list
        elif ">=" in condition1:
            smash=condition1.split(">=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]>=value:
                            list.append(y)
                    data[x]['values']=list
        elif "<" in condition1:
            smash=condition1.split("<")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]<value:
                            list.append(y)
                    data[x]['values']=list
        elif ">" in condition1:
            smash=condition1.split(">")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]>value:
                            list.append(y)
                    data[x]['values']=list
        elif "=" in condition1:
            smash=condition1.split("=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]==value:
                            list.append(y)
                    data[x]['values']=list
        list1=[]
        if "<=" in condition2:
            smash=condition2.split("<=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]<=value:
                            list1.append(y)
                    data[x]['values']=list1
        elif ">=" in condition2:
            smash=condition2.split(">=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]>=value:
                            list.append(y)
                    data[x]['values']=list1
        elif "<" in condition2:
            smash=condition2.split("<")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]<value:
                            list1.append(y)
                    data[x]['values']=list1
        elif ">" in condition2:
            smash=condition2.split(">")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]>value:
                            list1.append(y)
                    data[x]['values']=list1
        elif "=" in condition2:
            smash=condition2.split("=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]==value:
                            list1.append(y)
                    data[x]['values']=list1
        return data
    elif dic['cond'][0]==1 and dic['cond'][1]==0 and flag==1:
        condition1=dic['cond'][2]
        condition2=dic['cond'][3]
        list=[]
        list1=[]
        if "<=" in condition1:
            smash=condition1.split("<=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    store=x
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]<=value:
                            list.append(y)
                    #data[x]['values']=list
        elif ">=" in condition1:
            smash=condition1.split(">=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    store=x
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]>=value:
                            list.append(y)
                    #data[x]['values']=list
        elif "<" in condition1:
            smash=condition1.split("<")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    store=x
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]<value:
                            list.append(y)
                    #data[x]['values']=list
        elif ">" in condition1:
            smash=condition1.split(">")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    store=x
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]>value:
                            list.append(y)
                    #data[x]['values']=list
        elif "=" in condition1:
            smash=condition1.split("=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    store=x
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]==value:
                            list.append(y)
                    #data[x]['values']=list
        if "<=" in condition2:
            smash=condition2.split("<=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    store=x
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]<=value:
                            list1.append(y)
                    #data[x]['values']=list1
        elif ">=" in condition2:
            smash=condition2.split(">=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    store=x
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]>=value:
                            list.append(y)
                    #data[x]['values']=list1
        elif "<" in condition2:
            smash=condition2.split("<")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    store=x
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]<value:
                            list1.append(y)
                    #data[x]['values']=list1
        elif ">" in condition2:
            smash=condition2.split(">")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    store=x
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]>value:
                            list1.append(y)
                    #data[x]['values']=list1
        elif "=" in condition2:
            smash=condition2.split("=")
            col=smash[0]
            value=int(smash[1])
            for x in data:
                if data[x]['name']==table_name:
                    store=x
                    index=data[x]['columns'].index(col)
                    for y in data[x]['values']:
                        if y[index]==value:
                            list1.append(y)
                    #data[x]['values']=list1
        #list2=[]
        for x in list1:
            if x not in list:
                list.append(x)
        data[store]['values']=list
        return data
def cons(data,dic,table_name,condition1):
    list=[]
    if "<=" in condition1:
        smash=condition1.split("<=")
        col=smash[0]
        value=int(smash[1])
        for x in data:
            if data[x]['name']==table_name:
                index=data[x]['columns'].index(col)
                for y in data[x]['values']:
                    if y[index]<=value:
                        list.append(y)
                data[x]['values']=list
    elif ">=" in condition1:
        smash=condition1.split(">=")
        col=smash[0]
        value=int(smash[1])
        for x in data:
            if data[x]['name']==table_name:
                index=data[x]['columns'].index(col)
                for y in data[x]['values']:
                    if y[index]>=value:
                        list.append(y)
                data[x]['values']=list
    elif "<" in condition1:
        smash=condition1.split("<")
        col=smash[0]
        value=int(smash[1])
        for x in data:
            if data[x]['name']==table_name:
                index=data[x]['columns'].index(col)
                for y in data[x]['values']:
                    if y[index]<value:
                        list.append(y)
                data[x]['values']=list
    elif ">" in condition1:
        smash=condition1.split(">")
        col=smash[0]
        value=int(smash[1])
        for x in data:
            if data[x]['name']==table_name:
                index=data[x]['columns'].index(col)
                for y in data[x]['values']:
                    if y[index]>value:
                        list.append(y)
                data[x]['values']=list
    elif "=" in condition1:
        smash=condition1.split("=")
        col=smash[0]
        value=int(smash[1])
        for x in data:
            if data[x]['name']==table_name:
                index=data[x]['columns'].index(col)
                for y in data[x]['values']:
                    if y[index]==value:
                        list.append(y)
                data[x]['values']=list
    return data
