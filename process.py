def query_summary(query_array):
    # print(query_array)
    length=len(query_array)
    run=query_array[len(query_array)-1].split(';')
    query_array[len(query_array)-1]=run[0]
    dic=dict()
    if length==4:
        if "distinct" in query_array[1]:
            run=query_array[1].split("distinct(")
            #print(run)
            if len(run)==2:
                query_array[1]=run[1]
                run1=query_array[1].split(')')
                query_array[1]=run1[0]
                #print(query_array[1])
                col=[0]
                tables=[]
                col.append(query_array[1].split(','))
                tables=query_array[3].split(',')
                if run1[1]!='':
                    col[0]=3
                    sm=run[1].split(',')
                    col.append(sm[1])
                dic=dict(columns=col,tables=tables,distinct=1,cond=[])
                #print(dic)
                return dic
            elif len(run)>2:
                col=[1]
                #print(run)
                for x in run:
                    if ")," in x:
                        #print(x)
                        flag=x.split("),")
                        #print(flag)
                        col.append(flag[0])
                    elif ")" in x:
                        # print(x)
                        # print(flag)
                        flag=x.split(')')
                        #print(flag)
                        col.append(flag[0])
                    tables=query_array[3].split(',')
                dic=dict(columns=col,tables=tables,distinct=1,cond=[])
                #print(dic)
                return dic
        elif "distinct" not in query_array[1]:
            col=[-1]
            tables=[]
            col.append(query_array[1].split(','))
            tables=query_array[3].split(',')
            dic=dict(columns=col,tables=tables,distinct=0,cond=[])
            #print(dic)
            return dic
    elif length>4:
        if "distinct" in query_array[1]:
            run=query_array[1].split("distinct(")

            if len(run)==2:
                query_array[1]=run[1]
                run1=query_array[1].split(')')
                query_array[1]=run1[0]
                col=[0]
                tables=[]
                col.append(query_array[1].split(','))
                tables=query_array[3].split(',')
                if "and" in query_array:
                    cond=[]
                    cond.append(1)
                    cond.append(1)
                    cond.append(query_array[5])
                    cond.append(query_array[7])
                elif "AND" in query_array:
                    cond=[]
                    cond.append(1)
                    cond.append(1)
                    cond.append(query_array[5])
                    cond.append(query_array[7])
                elif "or" in query_array:
                    cond=[]
                    cond.append(1)
                    cond.append(0)
                    cond.append(query_array[5])
                    cond.append(query_array[7])
                elif "and" in query_array:
                    cond=[]
                    cond.append(1)
                    cond.append(0)
                    cond.append(query_array[5])
                    cond.append(query_array[7])
                else:
                    cond=[]
                    cond.append(0)
                    cond.append(0)
                    cond.append(query_array[5])
                dic=dict(columns=col,tables=tables,distinct=1,cond=cond)
                #print(dic)
                return dic
            elif len(run)>2:
                col=[1]
                for x in run:
                    if ")," in x:
                        flag=x.split("),")
                        col.append(flag[0])
                    elif ")" in x:
                        flag=x.split(')')
                        col.append(flag[0])
                    tables=query_array[3].split(',')
                if "and" in query_array:
                    cond=[]
                    cond.append(1)
                    cond.append(1)
                    cond.append(query_array[5])
                    cond.append(query_array[7])
                elif "AND" in query_array:
                    cond=[]
                    cond.append(1)
                    cond.append(1)
                    cond.append(query_array[5])
                    cond.append(query_array[7])
                elif "or" in query_array:
                    cond=[]
                    cond.append(1)
                    cond.append(0)
                    cond.append(query_array[5])
                    cond.append(query_array[7])
                elif "and" in query_array:
                    cond=[]
                    cond.append(1)
                    cond.append(0)
                    cond.append(query_array[5])
                    cond.append(query_array[7])
                else:
                    cond=[]
                    cond.append(0)
                    cond.append(0)
                    cond.append(query_array[5])
                dic=dict(columns=col,tables=tables,distinct=1,cond=cond )
                #print(dic)
                return dic
        elif "distinct" not in query_array[1]:
            col=[-1]
            tables=[]
            col.append(query_array[1].split(','))
            tables=query_array[3].split(',')
            if "and" in query_array:
                cond=[]
                cond.append(1)
                cond.append(1)
                cond.append(query_array[5])
                cond.append(query_array[7])
            elif "AND" in query_array:
                cond=[]
                cond.append(1)
                cond.append(1)
                cond.append(query_array[5])
                cond.append(query_array[7])
            elif "or" in query_array:
                cond=[]
                cond.append(1)
                cond.append(0)
                cond.append(query_array[5])
                cond.append(query_array[7])
            elif "and" in query_array:
                cond=[]
                cond.append(1)
                cond.append(0)
                cond.append(query_array[5])
                cond.append(query_array[7])
            else:
                cond=[]
                cond.append(0)
                cond.append(0)
                cond.append(query_array[5])
            dic=dict(columns=col,tables=tables,distinct=0,cond=cond)
            #print(dic)
            return dic
            
