import sys
import csv
import re
from check import query_check
from process import query_summary
from star import star
from minimax import mini,maxi,sum,average
from distinct import distinct
from project import project
from con import con,cons
from join import join

query=sys.argv[1]
query_array=re.findall(r'\S+',query)
data=dict()
file = open('./metadata.txt', 'r')
line = file.readline().strip()
while line:
	if line == "<begin_table>":
		tables = file.readline().strip()
		data[tables] = {}
		data[tables]['columns'] = []
		columns = file.readline().strip()
		while columns != "<end_table>":
			data[tables]['columns'].append(columns.lower())
			columns = file.readline()
			columns = columns.strip()
	line = file.readline().strip()

for tables in data:
	data[tables]['values'] = []
	data[tables]['name'] = tables
	f = open('./' + tables + '.csv', 'r')
	for line in f:
		arr = [int(field.strip('"')) for field in line.strip().split(',')]
		data[tables]['values'].append(arr)


def call(query,query_array):
	check_var=query_check(query,query_array)
	if check_var==1:
		less_length=len(query)
		length=len(query_array)
		dic=dict()
		dic=query_summary(query_array)
		# print(dic)
		if query_array[1]!="*":
			if "min" not in query_array[1]:
				if "max" not in query_array[1]:
					if "Sum" not in query_array[1]:
						if "average" not in query_array[1]:
							for x in data:
								if len(dic['tables'])==1:
									if x==dic['tables'][0]:
										for y in dic['columns'][1]:
											if y not in data[x]['columns']:
												print("ERROR COLUMN NOT IN TABLE")
												return
								elif len(dic['tables'])>1:
									if x==dic['tables'][0]:
										colss=dic['columns'][1]
										for y in colss[0]:
											if y not in data[x]['columns']:
												print("ERROR COLUMN NOT IN TABLE")
												return
									if x==dic['tables'][1]:
										colss=dic['columns'][1]
										for y in colss[1]:
											if y not in data[x]['columns']:
												print("ERROR COLUMN NOT IN TABLE")
												return

		if length>0 and query_array[1]!="*":
			if "min" in query_array[1]:
				mini(data,dic)
				return
	        if "max" in query_array[1]:
				maxi(data,dic)
				return
	        if "Sum" in query_array[1]:
				sum(data,dic)
				return
	        if "average" in query_array[1]:
				average(data,dic)
				return
		if (length>=6 and '.' not in dic['cond'][2]) or (length<6):
			if length>1 and query_array[1]!="*":
				if dic['distinct']==1:
					if dic['cond']==[]:
						distinct(data,dic)
						return
					if dic['cond']!=[]:
						data1=con(data,dic,dic['tables'][0],1)
						distinct(data1,dic)
						return
				elif dic['distinct']==0:
					if dic['columns'][0]== -1 and len(dic['columns'][1])>=1 and len(dic['tables'])==1:
						# print("yes")
						if dic['cond']==[]:
			  				for x in dic['columns'][1]:
			   					project(data,dic,x,dic['tables'][0])
						elif dic['cond']!=[]:
							data1=con(data,dic,dic['tables'][0],1)
							for x in dic['columns'][1]:
			   					project(data1,dic,x,dic['tables'][0])
			        if dic['columns'][0]== -1 and len(dic['columns'][1])>=1 and len(dic['tables'])>1:
						# print("yes")
						if dic['cond']!=[]:
							length=len(dic['tables'])
							check_len=len(dic['columns'][1])
							if length != check_len:
								print("SYNTAX ERROR")
								return
							list=dic['columns'][1]
							for x in range(length):
								data5=cons(data,dic,dic['tables'][x],dic['cond'][2+x])
								print(dic['tables'][x])
								project(data5,dic,list[x],dic['tables'][x])
						if dic['cond']==[]:
							length=len(dic['tables'])
							#length=len(dic['tables'])
							check_len=len(dic['columns'][1])
							if length != check_len:
								print("SYNTAX ERROR")
								return
							list=dic['columns'][1]
							for x in range(length):
								print(dic['tables'][x])
								project(data,dic,list[x],dic['tables'][x])
		if length>=5:
			if '.' in dic['cond'][2]:
			#("first check")
				join(data,dic)
			elif query_array[1]=="*":
				star(data,dic,query_array)
		if length<=4:
			if query_array[1]=="*":
				star(data,dic,query_array)

	# else:
	# 	print("ERROR:---- QUERY MISMATCH -- please check your query!!!")
	return

call(query,query_array)
